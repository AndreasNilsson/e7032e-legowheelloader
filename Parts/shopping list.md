<!-- {::options parse_block_html="true" /} -->

# Shopping list

## Actuators

* Motors with encoders
	* 4x motor: wheels
	* 1x motor: scoop
	* 1x motor: arm
* 2x servos for steering (if we choose to do it)

---

## Sensors
* 2x Current measurement sensor
* Motor encoders (Included inside the motors)

---

## Battery
* 12v Lipo

---

## Computer/Microconrollers
* Arduino MEGA
* RaspberryPI

---

## Other circuits and electronics stuff
* Step down Converter (input voltage: 8-36V, output voltage: 1.25-32V)
* Power distribution board (for connectiong the battery with respective part in a clean way.)

---

## Misc
* super glue

---

## Table of components and prices
| Component   |      Price   |  Quantity |  Link | Site | Approved by Jan | Ordered by Andreas | Recieved by Andreas|
|----------|:-------------|:--|:------| :-- |:-------  | :-----  | :------- |
| Raspberry Pi 4 1GB| 355kr | 1 | [Link](https://www.elfa.se/sv/raspberry-pi-5ghz-quad-core-1gb-ram-raspberry-pi-pi4-model-1gb/p/30152779?track=true) | Elfa |X | | |
| Arduino Mega | 331.96kr | 1 | [Link](https://www.elfa.se/sv/mikrostyrenhetskort-mega2560-r3-arduino-a000067/p/11038920?queryFromSuggest=true) | Elfa |X | | |
| Motor (210 RPM, 6V)| 98.09kr | 6 | [Link](https://www.banggood.com/sv/CHIHAI-MOTOR-GM25-370-DC-Gear-Motor-6V-100210300RPM-Encoder-Motor-p-1016183.html?gmcCountry=SE&currency=SEK&createTmp=1&utm_source=googleshopping&utm_medium=cpc_union&utm_content=xibei&utm_campaign=xibei-ssc-se-all-0716&ad_id=367117562532&gclid=CjwKCAjwtuLrBRAlEiwAPVcZBvmPD1cChYwrroFr_86E9S26UYTZOcAR2ORKHHgKESxtpb5Ba_H2ahoC0ucQAvD_BwE&ID=519630&cur_warehouse=CN) | Banggood |X | | |
| Motor controller (10 pack) | 120.38kr | 1 | [Link](https://www.banggood.com/sv/10pcs-Dual-Motor-Driver-Module-1A-TB6612FNG-For-Arduino-Microcontroller-p-1380808.html?rmmds=detail-left-hotproducts__2&cur_warehouse=CN) | Banggood | X | | |
| Battery ~ 12V <small>*(see comment right below this table)*</small> | 471.21kr | 1 | [Link](https://www.banggood.com/2Pcs-ZOP-Power-11_1V-5000mAh-60C-3S-Lipo-Battery-XT60-Plug-For-RC-Car-Quadcopter-p-1516605.html?rmmds=search&cur_warehouse=CN) | Banggood | X | | |
| Charger | 265.01kr | 1 | [Link](https://www.banggood.com/IMAX-B6-80W-6A-Lipo-Battery-Balance-Charger-with-Power-Supply-Adapter-p-1401686.html?rmmds=search&ID=47184&cur_warehouse=CN) | Banggood | X | | |
| Charging cable | 29.36kr | 1 | [Link](https://www.banggood.com/18AWG-4mm-XT60-Connector-to-Banana-Plug-Battery-Connectors-Charger-Cable-20cm-p-938171.html?rmmds=search&cur_warehouse=CN) | Banggood | X | | |
| Cable from battery | 20.52kr | 1 | [Link](https://www.banggood.com/XT60-Male-Plug-10AWG-10cm-With-Wire-p-77537.html?rmmds=search&cur_warehouse=CN) | Banggood | X | | |
| Step down converter USB | 28.28kr | 1 | [Link](https://www.banggood.com/sv/LM2596S-Dual-USB-port-9V12V24V36V-to-5V-DC-DC-Step-Down-Buck-Car-Charger-Solar-3A-Power-Supply-Module-p-1417089.html?rmmds=search&cur_warehouse=CN) | Banggood | X | | |
| Step down converter | 9.72kr | 4 | [Link](https://www.banggood.com/sv/DC-DC-7-28V-to-5V-3A-Step-Down-Power-Supply-Module-Buck-Converter-Replace-LM2596-p-1536688.html?rmmds=search&cur_warehouse=CN) | Banggood | X | | |
| Current measurement | 96.52kr | 2 | [Link](https://www.digikey.se/product-detail/en/adafruit-industries-llc/904/1528-1168-ND/5353628?cur=SEK&lang=en) | Digi-Key | X | | |
| Servo | 52.73kr | 2 | [Link](https://www.banggood.com/sv/MG995-High-Torgue-Mental-Gear-Analog-Servo-p-73885.html?rmmds=search&cur_warehouse=CN) | Banggood | X | | |
| USB cable Type-C | 29.36kr | 1 | [Link](https://www.banggood.com/sv/BlitzWolf-BW-TC13-3A-USB-Type-C-Charging-Data-Cable-0_98ft0_3m-For-Oneplus-6-Xiaomi-Mi8-Mix-2s-S9-p-1339805.html?rmmds=search&ID=224&cur_warehouse=CN) | Banggood | X | | |
| Super glue | 97.70 (15kr biltema) | 1 | [Link](https://www.elfa.se/en/instant-adhesive-loctite-loctite-454-3g-ch-de/p/11041259?track=true) | Elfa | X | | |
| Step down converter (5 pack) | 103.37kr | 1 | [Link](https://www.banggood.com/sv/5Pcs-XL4015-5A-DC-DC-Step-Down-Adjustable-Power-Supply-Module-Buck-Converter-p-1157549.html?rmmds=search-left-hotproducts__4&cur_warehouse=CN) | Banggood | X | | |
| Power distribution board | 12.68kr | 2 | [Link](https://www.banggood.com/30x30-35x35-PCB-ESC-Power-Distribution-Board-For-MINI-Quadcopter-Multicopter-p-984686.html?rmmds=search&cur_warehouse=CN) | Banggood | X | | |
| microSD card | 201kr | 1 | [Link](https://www.elfa.se/sv/extreme-pro-microsd-memory-card-32-gb-sandisk-sdsqxaf-032g-gn6ma/p/30110938?track=true) | Elfa | X | | |

***Comment:** The battery has it's nominall voltage 11.1V but when it is fully charged, it is 12.6V. When it is totaly empty, it is 9.9V.*

| C | P | Q | L | S | A | O | R |  <!--Component, Price, Quantitiy, Link, Site-name, Approval, Ordered, Recieved -->

--- 
