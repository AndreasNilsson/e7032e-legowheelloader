# Week 38:
This week Martin, Emil and Viktor are out traveling and will not be present in the project room.   

Goals for the week:

* Presentation
* List of components
	- order components...
* Personal description page
* Look at software solutions	
	* how to communicate
	* how coding should be structured
* Research about ROS (Emil + Linus)
* Start the modeling of wheel loader

#### Monday
Planned the week in Trello.  
Measured min/max torque for the arm and scoop to be able to choose the correct motors.  
Started including parts into the list of components


Work for tomorrow:

* Make a good presentation
* Finish list and order components

#### Tuesday


#### Wednesday


#### Thursday


#### Friday


# Week 37:
This week we need to complete the following:  

* Decide what components to order for the robot

#### Monday
Constructed Gantt, WBS, Presentation, Started looking for components and discussing ideas about the project.


#### Tuesday
Prepared for the presentation and presented the project at the lesson.  
Had a meeting after discussing how to proceed with the feedback we got.

Feedback:  

* Improve the flowchart, expand the project idea
* Send the GIT links to Jan (Done)
* Add description of each person on Project Git
* Make a sequence diagram, who is talking to who and how, for the different parts of the wheel loader(electronics) and network(control system + commands and fault detection)


#### Wednesday

Started on the Sequence diagram, but it needs much more work  
Started looking for more components and making sure that everything necessary is included.  
Found some libraries for Arduino/C++ for the encoders and motor drivers

Stuff that needs to be done:

* Make a list of components to hand over to Jan
* More planning of the project
* Finishing sequence diagram and flow chart

Questions that occurred:

* How much budget do we have? maximum/minimum?
* How do we measure the lift forces of the arm and scoop?
	- Can we measure the current in some way to estimate the lift force needed?
	- what sensors is already available?
	- Can we measure torque?
* How do we design the power supply system?




#### Thursday
Expanded flowcharts and constructed a Sequence diagram
Made a list of components
Research some motors and encoders and components for the project

Stuff that needs to be researched on Friday:

 *  Smaller motors? Hydraulic motors? (Martin)
 *  Measuring current draw from motors for estimating lift force (Adonay)
	 -  Tilt force
	 -  lift force
 *  List of components
 *  Making a GOOD presentation (Linus + Emil)
	 -  Order components after the presentation has been done
 *  Budget? What is our limits?
 *  Batteries? Two power supply sources
	 -  LiPo
	 -  Regular store batteries?
 *  

##### For the presentation:

* Talked to the french guy, Alban, about NX and so on
	- said it would take up to much time
* Introduce what sensor data the research team used(lift force, angular velocity, speed of vehicle, angular position)
* Talked to Khalid
	- Motor with encoder would work with control algorithms to control the arm and scoop and speed
	- estimating the lift force using current draw measurements from the motors(this is why we need Arduino for analog readings)
	- (would like to use hydraulics but...)
* Flow chart
	- describe each component
	- how they connect to each other
	- why we did it this way(batteries, Arduino, RasPi, motors, encoders, etc)
* Sequence diagram
	- How does each unit talk to one another
	- why we have chosen this communication(ROS from pc to RasPi, Serial RasPi-Arduino, sensor data, control processing, etc)
* List of potential failures(fault detection) and how to detect them and what to do when they occur(risk management, FMEA) 

#### Friday
Made some progress on looking for components.  
Made a presentation for next week.  
Started looking at Trello for planning tasks every week and how it is connected to Gantt and GIT  
GIT and group workflow is structurized.  

Needs to be done next week:

* Personal description page
* List of components
	- order components...
* Look at software solutions	
	- how to communicate
	* how coding should be structured
* Presentation
* Research about ROS (Emil + Linus)
* Modeling of wheel loader



